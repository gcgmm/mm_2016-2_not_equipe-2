(function () {


  //  setTimeout(function() { window.mostraPerguntaEquipe1() }, 3000);

  var equipe1 = getParameterByName("equipe1").split(",");
  var equipe2 = getParameterByName("equipe2").split(",");

  // document.getElementById('equipe1').innerHTML = "<h1>Equipe 1</h1> " + equipe1;
  // document.getElementById('equipe2').innerHTML = "<h1>Equipe 2</h1> " + equipe2;
  document.getElementById('equipe1').innerHTML = "";
  document.getElementById('equipe2').innerHTML = "";

  $('#vidasEquipe2').val(3);
  $('#vidasEquipe1').val(3);
  var questaoEquipe1;
  var questaoEquipe2;

  bonsai.setup({
    runnerContext: bonsai.IframeRunnerContext
  }).run(document.getElementById('equipe1'), {
    framerate: 40,
    code: function () {
      function isQuadradoEquipe1() {

        var x = r.attr('x');
        var x1 = r1.attr('x');
        var x2 = r2.attr('x');
        var x3 = r3.attr('x');
        var minX = x1;

        if (x < x1) {
          minX = x;
        }

        if (minX > x2) {
          minX = x2;
        }

        if (minX > x3) {
          minX = x3;
        }

        x = x - minX;
        x1 = x1 - minX;
        x2 = x2 - minX;
        x3 = x3 - minX;

        var qtXValido = 0;
        var vlMedia = 10;

        var xItem1;
        var xItem2;
        var xItemDiff1;
        var xItemDiff2;

        if (x >= 0 && x <= vlMedia) {
          qtXValido = qtXValido + 1;

          if (qtXValido == 1) {
            xItem1 = r;
          } else {
            xItem2 = r;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r;
          } else {
            xItemDiff2 = r;
          }
        }

        if (x1 >= 0 && x1 <= vlMedia) {
          qtXValido = qtXValido + 1;
          if (qtXValido == 1) {
            xItem1 = r1;
          } else {
            xItem2 = r1;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r1;
          } else {
            xItemDiff2 = r1;
          }
        }

        if (x2 >= 0 && x2 <= vlMedia) {
          qtXValido = qtXValido + 1;
          if (qtXValido == 1) {
            xItem1 = r2;
          } else {
            xItem2 = r2;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r2;
          } else {
            xItemDiff2 = r2;
          }
        }

        if (x3 >= 0 && x3 <= vlMedia) {
          qtXValido = qtXValido + 1;
          if (qtXValido == 1) {
            xItem1 = r3;
          } else {
            xItem2 = r3;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r3;
          } else {
            xItemDiff2 = r3;
          }
        }

        if (qtXValido == 2) {

          var y1 = xItem1.attr('y');
          var y2 = xItem2.attr('y');
          var minY;
          var continuar = false;
          var itemMin;
          var itemMax;

          if (y1 > y2) {
            var h1 = y1 - xItem2._attributes.height;
            minY = y2;
            itemMin = xItem2;
            itemMax = xItem1;
            continuar = (h1 >= (y2 - vlMedia) && h1 <= (y2 + vlMedia));
          } else {
            var h2 = y2 - xItem1._attributes.height;
            minY = y1;
            itemMin = xItem1;
            itemMax = xItem2;
            continuar = (h2 >= (y1 - vlMedia) && h2 <= (y1 + vlMedia));
          }

          if (continuar) {
            var y1Diff = xItemDiff1.attr('y');
            var y2Diff = xItemDiff2.attr('y');
            var itemValidar;
            var itemValidado;
            continuar = false;

            if (minY >= (y1Diff - vlMedia) && minY <= (y1Diff + vlMedia)) {
              var w1 = xItemDiff1.attr('x') - itemMin._attributes.width;
              continuar = (w1 >= (itemMin.attr('x') - vlMedia) && w1 <= (itemMin.attr('x') + vlMedia));
              itemValidar = xItemDiff2;
              itemValidado = xItemDiff1;
            } else if (minY >= (y2Diff - vlMedia) && minY <= (y2Diff + vlMedia)) {
              var w2 = xItemDiff2.attr('x') - itemMin._attributes.width;
              continuar = (w2 >= (itemMin.attr('x') - vlMedia) && w2 <= (itemMin.attr('x') + vlMedia));
              itemValidar = xItemDiff1;
              itemValidado = xItemDiff2;
            }

            if (continuar) {
              var diff = itemValidar.attr('y') - itemValidado._attributes.height;
              if (diff >= (itemValidado.attr('y') - vlMedia) && diff <= (itemValidado.attr('y') + vlMedia)) {
                diff = itemValidar.attr('x') - itemMax._attributes.width;
                if (diff >= (itemMax.attr('x') - vlMedia) && diff <= (itemMax.attr('x') + vlMedia)) {
                  window.parent.mostraPerguntaEquipe1();
                }
              }
            }
          }
        }
      }

      var MULTIPLIER = 1;

      new Rect(5, 5, 930 * MULTIPLIER, 950 * MULTIPLIER)
        //.stroke('#e6e6e6', 1)
        .addTo(stage)
      //    .attr('fillColor', 'green');


      var r = new Rect(300, 300, 300, 300);

      var controle = {};

      r.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 300,
        x: 300
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * -1,
          y: this.attr('y') * -1
        }

      })
        .on('multi:drag', function (e) {

          var obj = controle[e.touchId];

          var vx = (obj.x + e.diffX) * -1;
          var vy = (obj.y + e.diffY) * -1;

          if ((vx + 130) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 300) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * -1,
              y: (obj.y + e.diffY) * -1
            });
            isQuadradoEquipe1();
          }


        })
        .emit('multi:pointerdown');

      var r1 = new Rect(100, 300, 100, 300);

      r1.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 400,
        x: 50
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * -1,
          y: this.attr('y') * -1
        }

      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];

          var vx = (obj.x + e.diffX) * -1;
          var vy = (obj.y + e.diffY) * -1;

          if ((vx - 80) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 300) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * -1,
              y: (obj.y + e.diffY) * -1
            });
            isQuadradoEquipe1();
          }

        })
        .emit('multi:pointerdown');


      var r2 = new Rect(100, 300, 300, 100);

      r2.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 100,
        x: 100
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * -1,
          y: this.attr('y') * -1
        }

        x = this.attr('x') * -1;
        y = this.attr('y') * -1;
      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];
          var vx = (obj.x + e.diffX) * -1;
          var vy = (obj.y + e.diffY) * -1;

          if ((vx) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 100) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * -1,
              y: (obj.y + e.diffY) * -1
            });
            isQuadradoEquipe1();
          }

        })
        .emit('multi:pointerdown');

      var r3 = new Rect(100, 300, 100, 100);

      r3.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 800,
        x: 500
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * -1,
          y: this.attr('y') * -1
        }
        x = this.attr('x') * -1;
        y = this.attr('y') * -1;
      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];

          var vx = (obj.x + e.diffX) * -1;
          var vy = (obj.y + e.diffY) * -1;

          if ((vx + 0) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 100) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * -1,
              y: (obj.y + e.diffY) * -1
            });
            isQuadradoEquipe1();
          }



        })
        .emit('multi:pointerdown');

    },
    width: 950,
    height: 1000
  });

  bonsai.setup({
    runnerContext: bonsai.IframeRunnerContext
  }).run(document.getElementById('equipe2'), {
    framerate: 40,
    code: function () {

      var MULTIPLIER = 1;
      var controle = {};

      function isQuadradoEquipe2() {

        var x = r.attr('x');
        var x1 = r1.attr('x');
        var x2 = r2.attr('x');
        var x3 = r3.attr('x');
        var minX = x1;

        if (x < x1) {
          minX = x;
        }

        if (minX > x2) {
          minX = x2;
        }

        if (minX > x3) {
          minX = x3;
        }

        x = x - minX;
        x1 = x1 - minX;
        x2 = x2 - minX;
        x3 = x3 - minX;

        var qtXValido = 0;
        var vlMedia = 5;

        var xItem1;
        var xItem2;
        var xItemDiff1;
        var xItemDiff2;

        if (x >= 0 && x <= vlMedia) {
          qtXValido = qtXValido + 1;

          if (qtXValido == 1) {
            xItem1 = r;
          } else {
            xItem2 = r;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r;
          } else {
            xItemDiff2 = r;
          }
        }

        if (x1 >= 0 && x1 <= vlMedia) {
          qtXValido = qtXValido + 1;
          if (qtXValido == 1) {
            xItem1 = r1;
          } else {
            xItem2 = r1;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r1;
          } else {
            xItemDiff2 = r1;
          }
        }

        if (x2 >= 0 && x2 <= vlMedia) {
          qtXValido = qtXValido + 1;
          if (qtXValido == 1) {
            xItem1 = r2;
          } else {
            xItem2 = r2;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r2;
          } else {
            xItemDiff2 = r2;
          }
        }

        if (x3 >= 0 && x3 <= vlMedia) {
          qtXValido = qtXValido + 1;
          if (qtXValido == 1) {
            xItem1 = r3;
          } else {
            xItem2 = r3;
          }
        } else {
          if (!xItemDiff1) {
            xItemDiff1 = r3;
          } else {
            xItemDiff2 = r3;
          }
        }

        if (qtXValido == 2) {

          var y1 = xItem1.attr('y');
          var y2 = xItem2.attr('y');
          var minY;
          var continuar = false;
          var itemMin;
          var itemMax;

          if (y1 > y2) {
            var h1 = y1 - xItem2._attributes.height;
            minY = y2;
            itemMin = xItem2;
            itemMax = xItem1;
            continuar = (h1 >= (y2 - vlMedia) && h1 <= (y2 + vlMedia));
          } else {
            var h2 = y2 - xItem1._attributes.height;
            minY = y1;
            itemMin = xItem1;
            itemMax = xItem2;
            continuar = (h2 >= (y1 - vlMedia) && h2 <= (y1 + vlMedia));
          }

          if (continuar) {
            var y1Diff = xItemDiff1.attr('y');
            var y2Diff = xItemDiff2.attr('y');
            var itemValidar;
            var itemValidado;
            continuar = false;

            if (minY >= (y1Diff - vlMedia) && minY <= (y1Diff + vlMedia)) {
              var w1 = xItemDiff1.attr('x') - itemMin._attributes.width;
              continuar = (w1 >= (itemMin.attr('x') - vlMedia) && w1 <= (itemMin.attr('x') + vlMedia));
              itemValidar = xItemDiff2;
              itemValidado = xItemDiff1;
            } else if (minY >= (y2Diff - vlMedia) && minY <= (y2Diff + vlMedia)) {
              var w2 = xItemDiff2.attr('x') - itemMin._attributes.width;
              continuar = (w2 >= (itemMin.attr('x') - vlMedia) && w2 <= (itemMin.attr('x') + vlMedia));
              itemValidar = xItemDiff1;
              itemValidado = xItemDiff2;
            }

            // ate aqui validou os x iniciais e que no topo tem 2 itens com mesmo y
            // testar agora o item com o  x maior.
            if (continuar) {
              var diff = itemValidar.attr('y') - itemValidado._attributes.height;
              if (diff >= (itemValidado.attr('y') - vlMedia) && diff <= (itemValidado.attr('y') + vlMedia)) {
                diff = itemValidar.attr('x') - itemMax._attributes.width;
                if (diff >= (itemMax.attr('x') - vlMedia) && diff <= (itemMax.attr('x') + vlMedia)) {
                  window.parent.mostraPerguntaEquipe2();
                }
              }
            }
          }
        }
      }

      new Rect(5, 5, 920 * MULTIPLIER, 950 * MULTIPLIER)
        //.stroke('#e6e6e6', 1)
        .addTo(stage)

      var r = new Rect(300, 300, 300, 300);

      r.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 300,
        x: 300
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * 1,
          y: this.attr('y') * 1
        }

        x = this.attr('x') * 1;
        y = this.attr('y') * 1;
      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];

          var vx = (obj.x + e.diffX) * 1;
          var vy = (obj.y + e.diffY) * 1;

          if ((vx + 130) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 300) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * 1,
              y: (obj.y + e.diffY) * 1
            });
            isQuadradoEquipe2();
          }

        })
        .emit('multi:pointerdown');

      var r1 = new Rect(100, 300, 100, 300);

      r1.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 400,
        x: 50
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * 1,
          y: this.attr('y') * 1
        }

        x = this.attr('x') * 1;
        y = this.attr('y') * 1;
      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];

          var vx = (obj.x + e.diffX) * 1;
          var vy = (obj.y + e.diffY) * 1;

          if ((vx - 80) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 300) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * 1,
              y: (obj.y + e.diffY) * 1
            });
            isQuadradoEquipe2();
          }

        })
        .emit('multi:pointerdown');


      var r2 = new Rect(100, 300, 300, 100);

      r2.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 100,
        x: 100
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * 1,
          y: this.attr('y') * 1
        }

        x = this.attr('x') * 1;
        y = this.attr('y') * 1;
      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];
          var vx = obj.x + e.diffX;
          var vy = obj.y + e.diffY;

          if ((vx) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 100) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * 1,
              y: (obj.y + e.diffY) * 1
            });
            isQuadradoEquipe2();
          }

        })
        .emit('multi:pointerdown');

      var r3 = new Rect(100, 300, 100, 100);

      r3.fill(color('red').randomize('h')).stroke('#000', 1).addTo(stage).attr({
        y: 800,
        x: 500
      }).on('multi:pointerdown', function (e) {
        if (!e) return;
        controle[e.touchId] = {
          x: this.attr('x') * 1,
          y: this.attr('y') * 1
        }

        x = this.attr('x') * 1;
        y = this.attr('y') * 1;
      })
        .on('multi:drag', function (e) {
          var obj = controle[e.touchId];

          var vx = (obj.x + e.diffX) * 1;
          var vy = (obj.y + e.diffY) * 1;

          if ((vx + 0) < 760 && (vx > 0) &&
            (vy > 0) && (vy + 100) < 960) {
            this.attr({
              x: (obj.x + e.diffX) * 1,
              y: (obj.y + e.diffY) * 1
            });
            isQuadradoEquipe2();
          }
        })
        .emit('multi:pointerdown');

    },
    width: 930,
    height: 1000
  });

})();

function addTagEquipe2(val) {
  $('#respostaEquipe2').addTag(val);
}

function addTagEquipe1(val) {
  $('#respostaEquipe1').addTag(val);
}

function addNivel() {
  if ($('#nivel').val() == 3) {
    $('#nivel').val(1);
  } else {
    $('#nivel').val(parseInt($('#nivel').val()) + 1);
  }
}

function confereRespostaEquipe2(resposta) {
  var respostaEquipe = $('#respostaEquipe2').val().replace(/,/gi, '');
  if (respostaEquipe == resposta) {
    window.parabens('2');
    addNivel();
    window.parent.mostraPerguntaEquipe2();
  } else {
    window.perdeVida('2');
    var vidas = parseInt($('#vidasEquipe2').val());
    vidas = vidas - 1;
    if (vidas <= 0) {
      window.equipe1venceu('1');
    }
    $('#vidasEquipe2').val(vidas);
  }
  $('#mostravidasequipe2').html("<h1>Vocês possuem " + $('#vidasEquipe2').val() + " vidas</h1>")
}

function confereRespostaEquipe1(resposta) {
  var respostaEquipe = $('#respostaEquipe1').val().replace(/,/gi, '');
  if (respostaEquipe == resposta) {
    window.parabens('1');
    addNivel();
    window.parent.mostraPerguntaEquipe1();
  } else {
    window.perdeVida('1');
    var vidas = parseInt($('#vidasEquipe1').val());
    vidas = vidas - 1;
    if (vidas <= 0) {
      window.equipe2venceu('2');
    }
    $('#vidasEquipe1').val(vidas);
  }

  $('#mostravidasequipe1').html("<h1>Vocês possuem " + $('#vidasEquipe1').val() + " vidas</h1>")
}

function getQuestao() {
  var random = Math.floor((Math.random() * 4) + 1);
  if ($('#nivel').val() == '1') {
    return questoes.nivel1[random];
  } else if ($('#nivel').val() == '2') {
    return questoes.nivel2[random];
  } else {
    return questoes.nivel3[random];
  }
}

var questoes = {
  nivel1:
  [
    {
      pergunta: "x² +4x +4",
      resposta: "(x+2)²",
      opcoes: ["(", "-3x", "x", "+2", ")", "²", "³"]
    },
    {
      pergunta: "x² +6x +9",
      resposta: "(x+3)²",
      opcoes: [")", "-4", "+3", "(", "²", "-x", "x"]
    },
    {
      pergunta: "x² +12x +36",
      resposta: "(x+6)²",
      opcoes: [")", "+6", "+5", "(", "x", "-x", "²"]
    },
    {
      pergunta: "x² +10x +25",
      resposta: "(x+5)²",
      opcoes: ["(", "+5", "-5", ")", "²", "x", "-x"]
    }
  ],
  nivel2:
  [
    {
      pergunta: "x² +22x +121",
      resposta: "(x+11)²",
      opcoes: [")", "+11", "(", ")", "²", "+22", "x"]
    },
    {
      pergunta: "x² +24x +144",
      resposta: "(x+12)²",
      opcoes: ["+12", "x", "-x", ")", "²", "(", "+24"]
    },
    {
      pergunta: "x² +26x +169",
      resposta: "(x+13)²",
      opcoes: ["(", "+13", "+50.5", ")", "²", "x", "+12x"]
    },
    {
      pergunta: "x² +30x +225",
      resposta: "(x+15)²",
      opcoes: ["(", "+15", "+5x", ")", "²", "x", "30"]
    }
  ],
  nivel3:
  [
    {
      pergunta: "4x² +12x +9",
      resposta: "(2x+3)²",
      opcoes: ["(", "+4", "2", ")", "²", "x", "x²"]
    },
    {
      pergunta: "9x² +12x +4",
      resposta: "(3x+2)²",
      opcoes: ["(", "+9", ")", "²", "x", "+3", "3x"]
    },
    {
      pergunta: "4x² +12x +36",
      resposta: "(2x+6)²",
      opcoes: ["(", "+6", "+12x", ")", "²", "x", "2x"]
    },
    {
      pergunta: "16x² +120x +225",
      resposta: "(4x+15)²",
      opcoes: ["(", "4x", "+25", ")", "²", "+15", "5x"]
    }
  ]
};


window.mostraPerguntaEquipe2 = function mostraPerguntaEquipe2() {
  $('#perguntaEquipe2').show();
  $('#respostaEquipe2').tagsInput();
  var questao = getQuestao();

  try {
    questao.pergunta
  } catch (e) {
    window.mostraPerguntaEquipe2();
    return;
  }
  var pergunta = {
    titulo: "Qual a forma reduzida da equação? \n" + questao.pergunta,
    opcoes: questao.opcoes,
    resposta: questao.resposta
  }

  $('#containerrespostaequipe2').innerHTML = '';
  $('#tituloEquipe2')[0].innerHTML = '<h1>' + pergunta.titulo + '</h1>';
  $('#opcoesEquipe2 input').remove();
  $('#botao_resposta2').remove();
  $('#perguntaEquipe2 .tagsinput .tag').remove();
  for (var i = 0; i < pergunta.opcoes.length; i++) {
    let o = pergunta.opcoes[i];

    let opc = document.createElement("input");
    opc.className = "opcao";
    opc.type = "button";
    opc.value = o;
     opc.id = i + Math.random(3400);
    opc.onclick = function () {
    //  addTagEquipe2(o);
    }

    opc.addEventListener('touchstart', function(e) {

      //e.preventDefault();
      console.log('############################ fdsf')
      //this.click();
      addTagEquipe2(o);
    }, false);

    $('#opcoesEquipe2').append(opc);
    $('#mostravidasequipe2').html("<h1>Vocês possuem " + $('#vidasEquipe2').val() + " vidas</h1>")

  }

  let resposta = document.createElement("input");
  resposta.className = "botao_resposta";
  resposta.type = "button";
  resposta.id = "botao_resposta2";
  resposta.value = "PRONTO"
  resposta.onclick = function () {
    //confereRespostaEquipe2(pergunta.resposta);
  }

  resposta.addEventListener('touchstart', function(e) {
    //  e.preventDefault();
    //console.log('############################ fdsf')
    //this.click();
    confereRespostaEquipe2(pergunta.resposta);

  }, false);

  $('#containerrespostaequipe2').append(resposta);
}

window.perdeVida = function (equipe) {
  $('#equipe' + equipe).hide();
  $('#equipe' + equipe + 'Perdeu').show();
  $('#perguntaEquipe' + equipe).hide();

  setTimeout(function () {
    $('#equipe' + equipe + 'Perdeu').hide();
    $('#perguntaEquipe' + equipe).show();
    $('#equipe' + equipe).show();
  }, 1500);
}

window.parabens = function (equipe) {
  $('#equipe' + equipe + 'Acertou').show();
  $('#equipe' + equipe).hide();

  setTimeout(function () {
    $('#equipe' + equipe + 'Acertou').hide();
    $('#equipe' + equipe).show();
  }, 2000);
}

window.equipe1venceu = function () {
  $('#perguntaEquipe1').hide();
  $('#perguntaEquipe2').hide();
  $('#equipe1venceu').show();
}

window.equipe2venceu = function () {
  $('#perguntaEquipe1').hide();
  $('#perguntaEquipe2').hide();
  $('#equipe2venceu').show();
}

window.mostraPerguntaEquipe1 = function () {
  $('#perguntaEquipe1').show();

  $('#respostaEquipe1').tagsInput();

  var questao = getQuestao();
  try {
    questao.pergunta
  } catch (e) {
    window.mostraPerguntaEquipe1();
    return;
  }
  var pergunta = {
    titulo: "Qual a forma reduzida da equação? \n" + questao.pergunta,
    opcoes: questao.opcoes,
    resposta: questao.resposta
  }

  $('#tituloEquipe1')[0].innerHTML = '<h1>' + pergunta.titulo + '</h1>';
  $('#opcoesEquipe1').innerHTML = '';
  $('#opcoesEquipe1 input').remove();
  $('#botao_resposta1').remove();
  $('#perguntaEquipe1 .tagsinput .tag').remove();
  for (var i = 0; i < pergunta.opcoes.length; i++) {
    let o = pergunta.opcoes[i];

    let opc = document.createElement("input");
    opc.className = "opcao";
    opc.id = i + Math.random(3400);
    opc.type = "button";
    opc.value = o;
    opc.onclick = function () {
      //addTagEquipe1(o);
    }

    opc.addEventListener('touchstart', function(e) {
      //  e.preventDefault();
      console.log('############################ opcao')
      //this.click();
      addTagEquipe1(o);
    }, false);


    $('#opcoesEquipe1').append(opc)


  }

  let resposta = document.createElement("input");
  resposta.className = "botao_resposta";
  resposta.type = "button";
  resposta.id = "botao_resposta1";
  resposta.value = "PRONTO"
  resposta.onclick = function () {
  //  confereRespostaEquipe1(pergunta.resposta);
  }

  resposta.addEventListener('touchstart', function(e) {
    //  e.preventDefault();
    console.log('############################ fdsf')
    //this.click();
    confereRespostaEquipe1(pergunta.resposta);
  }, false);

  $('#containerrespostaequipe1').append(resposta);
  $('#mostravidasequipe1').html("<h1>Vocês possuem " + $('#vidasEquipe1').val() + " vidas</h1>")
}
